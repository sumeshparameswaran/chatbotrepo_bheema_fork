import * as actionTypes from "./actions";
import axios from "axios";

export const projectLoabbyUser = () => {
    console.log("projectLoabbyUser");
    return {
      type: actionTypes.PROJECT_LOAD_BY_USER,
      loading : true,
      error :  false,
      projects: null,
    };
  };

  export const projectLoadByUserSucces = (data) => {
    console.log("PROJECT_LOAD_BY_USER_SUCCESS");
    return {
      type: actionTypes.PROJECT_LOAD_BY_USER_SUCCESS,
      projects : data,
      loading : false,
      error :  false
    };
  };
 
  
  export const deleteProjectStart = () => {
    console.log("delete_Project_Start");
    return {
      type: actionTypes.PROJECT_DELETE,
      projects: null,
      loading : false,
      error :  false
    };
  };

  export const deleteProjectSuccess = (projects2) => {
    console.log("delete_Project_Success");
    return {
      type: 'DELETE_PROJECT_SUCESS',
      projects: projects2,
      loading : false,
      error :  false
    };
  };

  export const loadProjectFail = (error) => {
    return {
      type: actionTypes.PROJECT_LOAD_BY_USER_FAILED,
      error: error,
      loading : false,
      error :  true
    };
  };

 

  export const loadProjectbyuser = () =>{
      return dispatch => {
          dispatch(projectLoabbyUser);

          const user = {
            user_id: localStorage.getItem("id"),
          };
          axios({
            method: "post",
            url: "http://127.0.0.1:8000/projects/loadProjectsByUser",
            data: user,
          }).then((response) => {
           dispatch(projectLoadByUserSucces(response.data.projects));
           
          }).catch((error) => {
            dispatch(loadProjectFail(error));
          });;

          

      }
  }



  export const deleteProject = (deleteProject) => {
   return dispatch => {
    console.log(deleteProject.id);
    dispatch(deleteProjectStart());
    const project = {
      project_id: deleteProject.id,
    };
    axios({
      method: "post",
      url: "http://127.0.0.1:8000/projects/deleteProject",
      data: project,
    }).then((response) => {
       loadProjectbyuser()
    });
  }

  };
  
  