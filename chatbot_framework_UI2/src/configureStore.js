import { createStore, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducer from "./store/reducer";

export const ConfigureStore = () => {
  const composeEnhances =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(reducer, composeEnhances(applyMiddleware(thunk)));
  return store;
};
