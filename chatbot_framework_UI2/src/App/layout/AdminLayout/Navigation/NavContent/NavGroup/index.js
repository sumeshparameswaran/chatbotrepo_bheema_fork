import React from "react";
import Aux from "../../../../../../hoc/_Aux";
import NavCollapse from "./../NavCollapse";
import NavItem from "./../NavItem";
import { connect } from "react-redux";
import * as actions from "../../../../../../store/auth";

const navGroup = (props) => {
  let navItems = "";
  const logout = () => {
    props.onLogout();
  };
  if (props.group.children) {
    const groups = props.group.children;
    navItems = Object.keys(groups).map((item) => {
      item = groups[item];
      switch (item.type) {
        case "collapse":
          return <NavCollapse key={item.id} collapse={item} type="main" />;
        case "item":
          return <NavItem layout={props.layout} key={item.id} item={item} />;
        case "logout":
          return (
            <div key= {item.id} onClick={logout}>
              <NavItem layout={props.layout} key={item.id} item={item} />
            </div>
          );
        default:
          return false;
      }
    });
  }

  return (
    <Aux>
      <li key={props.group.id} className="nav-item pcoded-menu-caption">
        <label>{props.group.title}</label>
      </li>
      {navItems}
    </Aux>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    onLogout: () => dispatch(actions.authLogout()),
  };
};

export default connect(null, mapDispatchToProps)(navGroup);
